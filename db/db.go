package db

import (
	"fmt"
	"log"
	"population-growth-per-country-1950-to-2021/environment"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func ConnectDB() (db *gorm.DB) {
	//State: Sprintf string
	databaseDsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s TimeZone=%s",
		environment.Env.HOST_DB, environment.Env.USERNAME_DB, environment.Env.PASSWORD_DB, environment.Env.DATABASE_DB, environment.Env.PORT_DB, environment.Env.SSL_MODE, environment.Env.TIME_LOC_DB)

	db, err := gorm.Open(postgres.Open(databaseDsn), &gorm.Config{
		// Logger: logger.Default.LogMode(logger.Info),
	})
	if err != nil {
		log.Fatal(err)
	}
	return
}
