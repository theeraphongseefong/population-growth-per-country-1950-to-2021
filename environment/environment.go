package environment

import (
	"fmt"
	"reflect"

	"github.com/spf13/viper"
)

var Env Environment

type Environment struct {
	// -- Echo
	PORT string `mapstructure:"PORT"`

	HOST_DB     string `mapstructure:"HOST_DB"`
	DATABASE_DB string `mapstructure:"DATABASE_DB"`
	USERNAME_DB string `mapstructure:"USERNAME_DB"`
	PASSWORD_DB string `mapstructure:"PASSWORD_DB"`
	PORT_DB     string `mapstructure:"PORT_DB"`
	TIME_LOC_DB string `mapstructure:"TIME_LOC_DB"`
	SSL_MODE    string `mapstructure:"SSL_MODE"`

	URL_END_POINT string `mapstructure:"URL_END_POINT"`
}

// Function Load Environment
func Load() (*Environment, error) {
	//State: Load From File
	var env Environment
	viper.SetConfigFile(".env")
	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}
	errUnmarshal := viper.Unmarshal(&env)
	if errUnmarshal != nil {
		return nil, err
	}

	//State: Check ENV
	err = checkRequiredKeys(env)
	if err != nil {
		return nil, err
	}

	return &env, err
}

// Function Check Required Keys
func checkRequiredKeys(env Environment) error {
	envValue := reflect.ValueOf(env)
	envType := envValue.Type()

	for i := 0; i < envValue.NumField(); i++ {
		fieldValue := envValue.Field(i)
		fieldType := envType.Field(i)
		if fieldValue.Interface() == reflect.Zero(fieldValue.Type()).Interface() {
			return fmt.Errorf("%s environment variable is required", fieldType.Name)
		}
	}

	return nil
}
