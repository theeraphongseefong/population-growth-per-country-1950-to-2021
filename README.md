# population-growth-per-country-1950-to-2021
population growth per country 1950 to 2021 build with Golang


## Getting started

## Build With
* Echo - Go web framework
* Postgres - Database

## Installation
```
# Run
docker-compose up

# if change file docker-compose.yaml must build again
docker-compose up --build
```

## Note Must Have Env file
```
PORT=8080

# Database
HOST_DB="xxx"
DATABASE_DB="xxx"
USERNAME_DB="xxx"
PASSWORD_DB="xxx"
PORT_DB=5432
TIME_LOC_DB="Asia/Bangkok"
SSL_MODE=disable

URL_END_POINT="/populations?startYear=1950&endYear=2021"
```

## Authors
- https://gitlab.com/theeraphongseefong/