package api

import (
	"fmt"
	"net/http"
	"population-growth-per-country-1950-to-2021/db"
	"population-growth-per-country-1950-to-2021/environment"
	"strconv"

	"github.com/labstack/echo/v4"
)

// Population represents the population_and_demography table in the database
type Population struct {
	CountryName string `json:"country_name"`
	Population  int    `json:"population"`
}

// Route to get top 12 population data for each year
func CallApi(c echo.Context) error {
	// Extract parameters from query string
	startYear, _ := strconv.Atoi(c.QueryParam("startYear"))
	endYear, _ := strconv.Atoi(c.QueryParam("endYear"))

	// Validate startYear and endYear
	if startYear < 1950 || endYear > 2021 || startYear > endYear {
		return c.String(http.StatusBadRequest, "Invalid startYear or endYear")
	}

	//State Connect Database
	db := db.ConnectDB()

	// Query the database for population data
	result := make(map[int][]Population)
	for year := startYear; year <= endYear; year++ {
		var populations []Population
		if err := db.Table("population_and_demography").
			Select("country_name, population").
			Where(`year = ? AND country_name NOT IN ('Less developed regions', 'Less developed regions, excluding least developed countries', 'Asia (UN)', 'Less developed regions, excluding China', 'Upper-middle-income countries', 'More developed regions', 'Lower-middle-income countries', 'High-income countries', 'Europe (UN)', 'Africa (UN)', 'Least developed countries', 'Latin America and the Caribbean (UN)', 'Northern America (UN)', 'Low-income countries', 'Land-locked developing countries (LLDC)')`, year).
			Order("population desc").
			Limit(13).
			Find(&populations).Error; err != nil {
			return c.String(http.StatusInternalServerError, "Internal Server Error")
		}
		result[year] = populations
	}

	return c.JSON(http.StatusOK, result)
}

func FrontWeb(c echo.Context) error {
	html := fmt.Sprintf(`<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Population Data Bar Graph</title>
		<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
	</head>
	<body>
	
	<h1>Population Data Bar Graph</h1>
	
	<label for="yearRange">Select Year Range:</label>
	<input type="range" id="yearRange" min="1951" max="2021" step="1" value="1951">
	
	<p>Selected Year: <span id="selectedYear">1951</span></p>
	<button id="playButton">Play</button>
	
	<canvas id="populationChart" width="800" height="400"></canvas>
	
	<script>
		// Declare a variable to store the reference to the chart
		let populationChart;
	
		// Declare a variable to store the fetched data
		let fetchedData;
	
		// Function to fetch data from the API
		async function fetchData(startYear, endYear) {
			const url = "%s";
			const response = await fetch(url);
			const data = await response.json();
			return data;
		}
	
		// Function to update the chart based on the selected year
		async function updateChart(selectedYear) {
			// Check if data is already fetched
			if (!fetchedData) {
				// Fetch data if not already fetched
				fetchedData = await fetchData(1951, 2021);
			}
	
			const total = fetchedData[selectedYear][0].population;

			fetchedData[selectedYear].shift();
			const labels = fetchedData[selectedYear].map(country => country.country_name);
	
			const datasets = [{
				label: selectedYear + "   total:" + total,
				data: fetchedData[selectedYear].map(country => country.population),
				backgroundColor: 'rgba(75, 192, 192, 0.2)',
				borderColor: 'rgba(75, 192, 192, 1)',
				borderWidth: 1,
			}];
	
			const ctx = document.getElementById('populationChart').getContext('2d');
	
			// Destroy the existing chart before creating a new one
			if (populationChart) {
				populationChart.destroy();
			}
	
			populationChart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: labels,
					datasets: datasets,
				},
				options: {
					scales: {
						y: {
							beginAtZero: true,
						},
					},
				},
				// ตัวอย่างการกำหนดขนาด Canvas ใน JavaScript
		plugins: {
			legend: {
				display: true,
				position: 'top',
			},
			title: {
				display: true,
				text: 'Population Data Bar Graph',
			},
		},
		layout: {
			padding: {
				left: 50,
				right: 50,
				top: 50,
				bottom: 50,
			},
		},
			});
	
			// Update the Selected Year display
			const selectedYearDisplay = document.getElementById('selectedYear');
			selectedYearDisplay.textContent = selectedYear;
	
			// Update the slider bar value
			const yearRangeInput = document.getElementById('yearRange');
			yearRangeInput.value = selectedYear;
		}
	
		// Initial chart with default selected year
		updateChart(1951);
	
		// Update chart based on the selected year range
		const yearRangeInput = document.getElementById('yearRange');
		const selectedYearDisplay = document.getElementById('selectedYear');
	
		yearRangeInput.addEventListener('input', () => {
			const selectedYear = parseInt(yearRangeInput.value);
			selectedYearDisplay.textContent = selectedYear;
			updateChart(selectedYear);
		});
	
		// Play button functionality
		const playButton = document.getElementById('playButton');
		let currentYear = 1951;
		let playInterval;
	
		playButton.addEventListener('click', () => {
			if (playInterval) {
				// Stop the play if it's already playing
				clearInterval(playInterval);
				playInterval = null;
				return;
			}
	
			// Start the play, update the chart every second
			playInterval = setInterval(() => {
				if (currentYear <= 2021) {
					updateChart(currentYear);
					currentYear++;
				} else {
					// Stop playing when it reaches the end
					clearInterval(playInterval);
					playInterval = null;
				}
			}, 1000);
		});
	</script>
	
	</body>
	</html>
	`, environment.Env.URL_END_POINT)
	// ส่ง HTML โดยตรง
	return c.HTML(http.StatusOK, html)
}
